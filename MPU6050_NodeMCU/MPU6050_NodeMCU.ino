#include <ESP8266WiFi.h>  // Biblioteca para usar las funciones WiFi del módulo ESP8266
#include <Wire.h>         // Biblioteca de comunicación I2C

const int MPU_ADDR =      0x68; // MPU6050 Address (0x68)
const int WHO_AM_I =      0x75; // Registro de identificación del dispositivo
const int PWR_MGMT_1 =    0x6B; // Registro de configuración del gestor de energía
const int GYRO_CONFIG =   0x1B; // Registro de configuración del giroscopio
const int ACCEL_CONFIG =  0x1C; // Registro de configuración del acelerómetro
const int ACCEL_XOUT =    0x3B; // Registro de lectura del eje X del acelerómetro

const int sda_pin = D5; // I2C SDA pin
const int scl_pin = D6; // I2C SCL pin

bool led_state = false;

// Variables para almacenar datos en bruto
int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;

//Variables para almacenar datos procesados
int16_t ArX, ArY, ArZ;
float Vmg; 

//Variables de condicionamiento de flujo
int watchdog;

// Definiciones de red WiFi
const char* SSID = "AVG";
const char* PASSWORD = "DDDDDAAAAA00000DDDDDAAAAA00000";

// Dirección del host de Adoras
const char* adorasHost = "10.100.200.195";  


WiFiClient client;

/*
 * Configura I2C con los pines deseados
 * sda_pin -> D5
 * scl_pin -> D6
 */
void initI2C() 
{
  //Serial.println("---inside initI2C");
  Wire.begin(sda_pin, scl_pin);
}

/*
 * Función que escribe un valor (val) en un registro (reg) dado.
 */
void writeRegMPU(int reg, int val)
{
  Wire.beginTransmission(MPU_ADDR);     // Inicia comunicación contra dirección del MPU6050
  Wire.write(reg);                      // Envía el registro con el cual se desea trabajar
  Wire.write(val);                      // Escribe el valor en el registro
  Wire.endTransmission(true);           // Fin de transmisión
}

/*
 * Función para lectura de un registro dado
 */
uint8_t readRegMPU(uint8_t reg)
{
  uint8_t data;
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(reg);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_ADDR, 1);
  data = Wire.read();
  return data;
}

/*
 * Busca el sensor en la dirección 0x68
 */
void findMPU(int mpu_addr)
{
  Wire.beginTransmission(MPU_ADDR);
  int data = Wire.endTransmission(true);

  if(data == 0)
  {
    Serial.print("Dispositivo encontrado en dirección: 0x");
    Serial.println(MPU_ADDR, HEX);
  }
  else 
  {
    Serial.println("¡Dispositivo no encontrado!");
  }
}

/*
 * Verifica que el sensor responda y esté activo
 */
void checkMPU(int mpu_addr)
{
  findMPU(MPU_ADDR);
    
  int data = readRegMPU(WHO_AM_I); // Register 117 – Who Am I - 0x75
  
  if(data == 104) 
  {
    Serial.println("MPU6050 en modo 104 (OK!)");

    data = readRegMPU(PWR_MGMT_1); // Register 107 – Power Management 1-0x6B

    if(data == 64) Serial.println("MPU6050 en modo 64 (SLEEP!)");
    else Serial.println("MPU6050 en modo ACTIVE!"); 
  }
  else Serial.println("Verifique dispositivo - MPU6050 no disponible!");
}

/*
 * Inicialización del sensor
 */
void initMPU()
{
  setSleepOff();
  setGyroScale();
  setAccelScale();
}

/* 
 *  Configurar sleep bit
 */
void setSleepOff()
{
  writeRegMPU(PWR_MGMT_1, 0); // Escribe 0 en el registro del gestor de energía (0x68), colocando el sensor en modo ACTIVE
}

/* Configuración de las escalas del giroscopio
   Registro de escalas del giroscopio: 0x1B[4:3]
   0 é 250°/s

    FS_SEL  Full Scale Range
      0        ± 250 °/s      0b00000000
      1        ± 500 °/s      0b00001000
      2        ± 1000 °/s     0b00010000
      3        ± 2000 °/s     0b00011000
*/
void setGyroScale()
{
  writeRegMPU(GYRO_CONFIG, 0);
}

/* Configuración de las escalas del acelerómetro
   Registro de escalas del acelerómetro: 0x1C[4:3]
   0 é 250°/s

    AFS_SEL   Full Scale Range    Counts/g
      0           ± 2g            16 384     0b00000000
      1           ± 4g             8 192     0b00001000
      2           ± 8g             4 096     0b00010000
      3           ± 16g            2 048     0b00011000
*/
void setAccelScale()
{
  writeRegMPU(ACCEL_CONFIG, 0);
}

/* Lectura de datos brutos del sensor (RAW)
   son 14 bytes en total siendo 2 bytes para cada eje y 2 bytes para la temperatura:

  0x3B 59 ACCEL_XOUT[15:8]
  0x3C 60 ACCEL_XOUT[7:0]
  0x3D 61 ACCEL_YOUT[15:8]
  0x3E 62 ACCEL_YOUT[7:0]
  0x3F 63 ACCEL_ZOUT[15:8]
  0x40 64 ACCEL_ZOUT[7:0]

  0x41 65 TEMP_OUT[15:8]
  0x42 66 TEMP_OUT[7:0]

  0x43 67 GYRO_XOUT[15:8]
  0x44 68 GYRO_XOUT[7:0]
  0x45 69 GYRO_YOUT[15:8]
  0x46 70 GYRO_YOUT[7:0]
  0x47 71 GYRO_ZOUT[15:8]
  0x48 72 GYRO_ZOUT[7:0]
   
*/
float readRawMPU()
{  
  Wire.beginTransmission(MPU_ADDR);       // Inicio comm. con dirección del MPU6050
  Wire.write(ACCEL_XOUT);                 // Envía el registro con el cual se desea trabajar, comenzando con el registro 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);            // termina la transmisión pero continúa con I2C abierto (envía STOP e START)
  Wire.requestFrom(MPU_ADDR, 14);         // configura para recibir 14 bytes de inicio del registro elegido arriba (0x3B)

  AcX = Wire.read() << 8;                 // lee primero el bit más significativo
  AcX |= Wire.read();                     // después el bit menos significativo
  AcY = Wire.read() << 8;
  AcY |= Wire.read();
  AcZ = Wire.read() << 8;
  AcZ |= Wire.read();

  Tmp = Wire.read() << 8;
  Tmp |= Wire.read();

  GyX = Wire.read() << 8;
  GyX |= Wire.read();
  GyY = Wire.read() << 8;
  GyY |= Wire.read();
  GyZ = Wire.read() << 8;
  GyZ |= Wire.read(); 

  ArX = (180/3.141592) * atan(AcX / sqrt(pow(AcY, 2) + pow(AcZ, 2)));
  ArY = (180/3.141592) * atan(AcY / sqrt(pow(AcX, 2) + pow(AcZ, 2)));
  ArZ = (180/3.141592) * atan(sqrt(pow(AcY, 2) + pow(AcX, 2)) / AcZ);

  Serial.print(sqrt(pow(ArX, 2) + pow(ArY, 2) + pow(ArZ, 2)));
  Serial.println(",");


/*
  //Serial.print("AcX = ");
  Serial.print(AcX);
  Serial.print(",");
  
  //Serial.print(" | AcY = ");
  Serial.print(AcY);
  Serial.print(",");

 // Serial.println(Vmg); //Vector magnitude
  
  //Serial.print(" | AcZ = ");
  //Serial.println(AcZ);
  //Serial.print(",");

  
  //Serial.print(" | Tmp = "); 
  //Serial.print(Tmp/340.00+36.53);
/*  
  //Serial.print(" | GyX = "); 
  Serial.print(GyX);
  Serial.print(",");
  
  //Serial.print(" | GyY = ");
  Serial.print(GyY);
  Serial.print(",");
  
  //Serial.print(" | GyZ = ");
  Serial.println(GyZ);
*/
  led_state = !led_state;
  digitalWrite(LED_BUILTIN, led_state);         // pisca LED do NodeMCU a cada leitura do sensor
  delay(50);                                        
  return sqrt(pow(ArX, 2) + pow(ArY, 2) + pow(ArZ, 2));
}

/*
 * Establece conexión con la red WiFi
 * SSID y PASSWORD requieren ser configurados en la declaración de variables global
 */
void reconnectWiFi() 
{
  if(WiFi.status() == WL_CONNECTED)
    return;

  WiFi.begin(SSID, PASSWORD);

  while(WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }

  Serial.println();
  Serial.print("Conectado correctamente a: ");
  Serial.println(SSID);
  Serial.print("IP obtenida: ");
  Serial.println(WiFi.localIP());  
}

void initWiFi()
{
  delay(10);
  Serial.print("Conectando a: ");
  Serial.println(SSID);
  Serial.println("Espere");

  reconnectWiFi();
}

void makePOST()
{
  if(!client.connect(adorasHost, 80))     //Conexión al servidor a través del puerto 80
  {
    Serial.println("Imposible conectar con el servidor\n");
  }
  else
  {    
    Serial.println("Conectado al servidor, enviando petición.");
    // Make HTTP POST request    
    client.println("GET /bootstrap/phydoras/api.php?action=physicalInterface HTTP/1.0");
    client.println();
  }
}


bool checkAlive()
{
  int now = millis();
  Serial.println("PLEASE, CONFIRM YOU'RE ALIVE.");
  while ((millis() - now) < 5000)
  {
    digitalWrite(0, HIGH);
    if (analogRead(0) > 750)
    {
      digitalWrite(0, LOW);
      return true;  
    }
    delay(10);
    yield();  
  }
  digitalWrite(0, LOW);
  return false;
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(0, OUTPUT);
  pinMode(4, OUTPUT);
  digitalWrite(0, LOW);
  digitalWrite(4, LOW);
  Serial.begin(115200);

  Serial.println("\nConfigurando WiFi\n");
  initWiFi();

  Serial.println("\nConfigurando MPU6050\n");
  initI2C();
  initMPU();
  checkMPU(MPU_ADDR);

  Serial.println("\nSistemas configurados. Iniciando servicio...\n");  

}

void loop() {
  if (readRawMPU() > 17)
  {
    watchdog++;  
  }
  else
  {
    watchdog = 0;
  }

  if (watchdog > 30)
  {
    if (!checkAlive())
    {
      digitalWrite(4, HIGH);
      Serial.println("*Sad Chopin music plays...*");
      Serial.println("DRIVER PROBABLY DIED :(");
      
      makePOST();
      
      while(true)
      {
        if (analogRead(0) > 750)
        {
          watchdog = 0;
          digitalWrite(4, LOW);
          return;  
        }
        yield();
        delay(10);
      }
    }
    watchdog = 0;  
  }
  delay(100);  
}

