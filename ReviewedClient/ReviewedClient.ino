/**
 * BasicHTTPClient.ino
 *
 *  Created on: 24.05.2015
 *
 */

const char* ssid = "AVG";
const char* password = "DDDDDAAAAA00000DDDDDAAAAA00000";

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>

#define USE_SERIAL Serial

boolean notified = false;

ESP8266WiFiMulti WiFiMulti;

void sendRequest();

void setup() {

    USE_SERIAL.begin(115200);
   // USE_SERIAL.setDebugOutput(true);

    USE_SERIAL.println();
    USE_SERIAL.println();
    USE_SERIAL.println();

    for(uint8_t t = 4; t > 0; t--) {
        USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
        USE_SERIAL.flush();
        delay(1000);
    }

    WiFiMulti.addAP(ssid, password);

    while (WiFiMulti.run() != WL_CONNECTED)
    {
      delay(500);
      USE_SERIAL.printf(".");  
    }
}

void loop() {
  if (analogRead(0) > 600)
  {
    if (!notified)
    {
      USE_SERIAL.printf("send\n");
      sendRequest();
      delay(150);
      yield(); 
    }
    USE_SERIAL.printf("wait\n");
    delay(15);
    yield();
  }
  else
  {
    notified = false;
    USE_SERIAL.printf("min\n");
    delay(15);
    yield();  
  }
}

void sendRequest()
{
// wait for WiFi connection
    if((WiFiMulti.run() == WL_CONNECTED)) {

        HTTPClient http;

        USE_SERIAL.print("[HTTP] begin...\n");
        http.begin("http://10.100.200.178/bootstrap/phydoras/api.php?action=physicalInterface"); //HTTP

        USE_SERIAL.print("[HTTP] GET...\n");
        // start connection and send HTTP header
        int httpCode = http.GET();

        // httpCode will be negative on error
        if(httpCode > 0) {
            // HTTP header has been send and Server response header has been handled
            USE_SERIAL.printf("[HTTP] GET... code: %d\n", httpCode);

            // file found at server
            if(httpCode == HTTP_CODE_OK) {
                String payload = http.getString();
                USE_SERIAL.println(payload);
                notified = true;
                USE_SERIAL.printf("SENT\n");
            }
        } else {
            USE_SERIAL.printf("FAIL\n");
            //USE_SERIAL.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }

        http.end();
    } else {
      USE_SERIAL.printf("FAIL_2\n");  
    } 
}

