/**
 * BasicHTTPClient.ino
 *
 *  Created on: 24.05.2015
 *
 */

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>

#define USE_SERIAL Serial

ESP8266WiFiMulti WiFiMulti;

void makeRequest(int verticality);

void setup() {

    USE_SERIAL.begin(115200);
   // USE_SERIAL.setDebugOutput(true);

    USE_SERIAL.println();
    USE_SERIAL.println();
    USE_SERIAL.println();

    for(uint8_t t = 4; t > 0; t--) {
        USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
        USE_SERIAL.flush();
        delay(1000);
    }
    const char* ssid = "AVG";
    const char* password = "DDDDDAAAAA00000DDDDDAAAAA00000";
    WiFiMulti.addAP(ssid, password);

}

void loop() {

    int verticality = analogRead(0);
    if (verticality > 800)
    {
      makeRequest(verticality);  
    }
    yield();
    delay(50);
}

void makeRequest(int verticality)
{
  // wait for WiFi connection
    if((WiFiMulti.run() == WL_CONNECTED))
    {
        HTTPClient http;
        USE_SERIAL.print("[HTTP] begin...\n");
        String requestUrl = "http://10.100.200.178/phydoras/index.php?verticality=" + String(verticality);
        http.begin(requestUrl);
        USE_SERIAL.print("[HTTP] GET...\n");
        // start connection and send HTTP header
        int httpCode = http.GET();
        // httpCode will be negative on error
        if(httpCode > 0)
        {
            // HTTP header has been send and Server response header has been handled
            USE_SERIAL.printf("[HTTP] GET... code: %d\n", httpCode);

            // file found at server
            if(httpCode == HTTP_CODE_OK)
            {
                String payload = http.getString();
                USE_SERIAL.println(payload);
            }
        }
        else
        {
            USE_SERIAL.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }

        http.end();
    }
    
  return;  
}
